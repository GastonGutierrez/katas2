package practica2;

class Node<T> {
    private T data;
    private Node<T> next;

    public Node(T number, T data) {
        this.data = data;

    }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }

}